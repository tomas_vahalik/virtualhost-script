#!/usr/bin/bash

# quick and dirty work
# make new virtual host.. simple and easy
# for licence info check licence file
# made by me for myself ;)

ROOT_UID="0"

#R U ROOT?
if [ "$UID" -ne "$ROOT_UID" ] ; then
  echo "You have to be root pal, sorry.."
  exit 1
fi

#root is ok :)

if [ "$1" ] ; then
  HOSTING_NAME="$1"
else
  echo "web name:"
  read answer
  echo ""
  if [ "${#answer}" -lt 2 ] ; then #less than 2 - again
    echo "Name is too short!"
    exit 1
  fi
  HOSTING_NAME="$answer"
fi

echo "creating directory.."
if [ -f /var/www/html/$HOSTING_NAME ] ; then
  echo "Folder with this name already exists.."
  exit 1
fi

mkdir /var/www/html/$HOSTING_NAME
mkdir /var/www/html/$HOSTING_NAME/www
chown -R straiki /var/www/html/$HOSTING_NAME
chgrp -R straiki /var/www/html/$HOSTING_NAME

echo "creating subdomain.."
cat >> /etc/httpd/conf/httpd.conf << EOF

<VirtualHost 127.0.0.1:80>
    ServerAdmin tomas@vahalik.cz
    DocumentRoot /var/www/html/$HOSTING_NAME/www
    ServerName $HOSTING_NAME.local
</VirtualHost>
EOF

echo "creating hosts link.."

echo "127.0.0.1      $HOSTING_NAME.local" >> /etc/hosts

echo "restarting server.."
service httpd restart

